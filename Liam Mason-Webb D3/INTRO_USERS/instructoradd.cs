﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class instructoradd : Form
    {
        public instructoradd()
        {
            InitializeComponent();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            Hide();
            adminconsole adminconsole = new adminconsole();
            adminconsole.ShowDialog();
            Close();
        }

        private void buttonRegister_Click(object sender, EventArgs e)
        {
            string username = "", password = "", firstname = "", lastname = "", phone = "", email = "";

            //Check that the text boxes has something typed in it using a method
            
            


            //(1) GET the data from the textboxes and store into variables created above, good to put in a try catch with error message
            try
            {
                /*YOUR CODE HERE*/
                username = textBoxUserName.Text.Trim();
                password = textBoxPassword.Text.Trim();
                firstname = textBoxFirst.Text.Trim();
                lastname = textBoxMiddle.Text.Trim();
                phone = textBoxLast.Text.Trim();
                email = textBox1.Text.Trim();
            }
            catch
            {
                //Error message, more useful when you are storing numbers etc. into the database.
                MessageBox.Show("Please make sure your text is in correct format.");
                return;
            }

            //(2) Execute the INSERT statement, making sure all quotes and commas are in the correct places.
            //      Practice first on SQL Server Management Studio to make sure it is entering the correct data and in the correct format,
            //      then copy across the statement and where there are string replace the actual text for the variables stored above.
            //Example query: " INSERT INTO Users VALUES ('jkc1', 'John', 'Middle', 'Carter', 'pass1') "
            try
            {
                SQL.executeQuery("INSERT INTO instructor VALUES ('" + username + "', '" + firstname + "', '" + email + "', '" + lastname + "','" + password + "', '" + phone + "')");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }



            //success message for the user to know it worked
            MessageBox.Show("Successfully Registered: " + firstname + " " + lastname + ". Your username is: " + username);

            //Go back to the login page since we registered successfully to let the user log in
            Hide();                                 //hides the register form
            LoginPage login = new LoginPage();      //creates the login page as an object
            login.ShowDialog();                     //shows the new login page form
            this.Close();                           //closes the register form that was hidden
        }
    }
    }

