﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class avi : Form
    {
        public avi()
        {
            InitializeComponent();
            String usernameQuery = "SELECT UserName From instructor";
            String dayQuery = "SELECT name FROM timeslot";
           
            SQL.editComboBoxItems(comboBoxuser, usernameQuery);
            SQL.editComboBoxItems(comboBoxday, dayQuery);
           


        }

        private void GO_Click(object sender, EventArgs e)
        {
            string username = "", day = "", time = "", id = "";

            //Check that the text boxes has something typed in it using a method


            try
            {
                /*YOUR CODE HERE*/
                username = comboBoxuser.Text.Trim();            
                day = comboBoxday.Text.Trim();
                id = textBoxid.Text.Trim();
            }
            catch
            {
                //Error message, more useful when you are storing numbers etc. into the database.
                MessageBox.Show("Please make sure your text is in correct format.");
                return;
            }

            //(2) Execute the INSERT statement, making sure all quotes and commas are in the correct places.
            //      Practice first on SQL Server Management Studio to make sure it is entering the correct data and in the correct format,
            //      then copy across the statement and where there are string replace the actual text for the variables stored above.
            //Example query: " INSERT INTO Users VALUES ('jkc1', 'John', 'Middle', 'Carter', 'pass1') "
            try
            {
                SQL.executeQuery("INSERT INTO avalbility VALUES ('" + id + "', '" + username + "', '" + day + "')");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }



            //success message for the user to know it worked//
            MessageBox.Show("YOU HAVE INDICATED YOU ARE AVALIBLE FOR ('" + day + "')");
        }

        private void back_Click(object sender, EventArgs e)
        {

        }
    }
}
