﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class inslog : Form
    {
        public inslog()
        {
            InitializeComponent();
            //This line of code allows us to obscure the password visually and limit the max chars in textbox
            password2.PasswordChar = '*';     
            password2.MaxLength = 20;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool loggedIn = false;
            string username = "", firstname = "", lastname = "", password = "";

            //check if boxes are empty, the Trim removes white space in text from either side
            if ("".Equals(username1.Text.Trim()) || "".Equals(password2.Text.Trim()))
            {
                MessageBox.Show("Please make sure you enter a Username and Password");
                return;
            }

            //(1) GET the username and password from the text boxes, is good to put them in a try catch
            try
            {
                /*YOUR CODE HERE*/

                username = username1.Text.Trim();
                password = password2.Text.Trim();



            }
            catch
            {
                //Error message, more useful when you are storing numbers etc. into the database.
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }

            //(2) SELECT statement getting all data from users, i.e. SELECT * FROM Users
            /*YOUR CODE HERE*/

            SQL.selectQuery("SELECT * FROM instructor");


            //(3) IF it returns some data, THEN check each username and password combination, ELSE There are no registered users
            /*YOUR CODE HERE*/
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (username.Equals(SQL.read[0].ToString()) && password.Equals(SQL.read[4].ToString()))
                    {
                        loggedIn = true;
                        firstname = SQL.read[1].ToString();
                        lastname = SQL.read[1].ToString();
                        break;
                    }
                }
            }

            else
            {
                MessageBox.Show("No users have been registered");
                return;
            }

            //if logged in display a success message
            if (loggedIn)
            {
                //message stating we logged in good
                Hide();
                avi avi = new avi();
                avi.ShowDialog();
                Close();
            }
            else
            {
                //message stating we couldn't log in
                MessageBox.Show("Login attempt unsuccessful! Please check details");
                username1.Focus();
                return;
            }
        }
        public void initialiseTextBoxes()
        {
            //goes through and clears all of the textboxes
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    (c as TextBox).Clear();
                }
            }
            //makes next place user types the text box
            username1.Focus();
        }

        private void Back_Click(object sender, EventArgs e)
        {
            Hide();
            LoginPage login = new LoginPage();
            login.ShowDialog();
            Close();
        }
    }
}
