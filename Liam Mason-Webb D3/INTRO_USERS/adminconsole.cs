﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class adminconsole : Form
    {
        public adminconsole()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Hide();
            LoginPage login = new LoginPage();
            login.ShowDialog();
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Hide();
            instructoradd add = new instructoradd();
            add.ShowDialog();
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hide();
            avi avi = new avi();
            avi.ShowDialog();
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
            caravi avi = new caravi();
            avi.ShowDialog();
            Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
           
        }
    }
}
