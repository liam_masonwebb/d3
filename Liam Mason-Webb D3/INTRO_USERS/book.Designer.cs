﻿namespace INTRO_USERS
{
    partial class book
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxins = new System.Windows.Forms.ComboBox();
            this.comboBoxtime = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxuser = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonbook = new System.Windows.Forms.Button();
            this.logout = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // comboBoxins
            // 
            this.comboBoxins.FormattingEnabled = true;
            this.comboBoxins.Location = new System.Drawing.Point(150, 92);
            this.comboBoxins.Name = "comboBoxins";
            this.comboBoxins.Size = new System.Drawing.Size(121, 21);
            this.comboBoxins.TabIndex = 0;
            // 
            // comboBoxtime
            // 
            this.comboBoxtime.FormattingEnabled = true;
            this.comboBoxtime.Location = new System.Drawing.Point(150, 135);
            this.comboBoxtime.Name = "comboBoxtime";
            this.comboBoxtime.Size = new System.Drawing.Size(121, 21);
            this.comboBoxtime.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(155, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Book Lession";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // textBoxuser
            // 
            this.textBoxuser.Location = new System.Drawing.Point(160, 187);
            this.textBoxuser.Name = "textBoxuser";
            this.textBoxuser.Size = new System.Drawing.Size(100, 20);
            this.textBoxuser.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Select Instructor";
            this.label2.Click += new System.EventHandler(this.label1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 20);
            this.label3.TabIndex = 1;
            this.label3.Text = "Select time";
            this.label3.Click += new System.EventHandler(this.label1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 187);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 20);
            this.label4.TabIndex = 1;
            this.label4.Text = "Your User Name";
            this.label4.Click += new System.EventHandler(this.label1_Click);
            // 
            // buttonbook
            // 
            this.buttonbook.Location = new System.Drawing.Point(325, 254);
            this.buttonbook.Name = "buttonbook";
            this.buttonbook.Size = new System.Drawing.Size(75, 23);
            this.buttonbook.TabIndex = 3;
            this.buttonbook.Text = "Book";
            this.buttonbook.UseVisualStyleBackColor = true;
            this.buttonbook.Click += new System.EventHandler(this.buttonbook_Click);
            // 
            // logout
            // 
            this.logout.Location = new System.Drawing.Point(12, 254);
            this.logout.Name = "logout";
            this.logout.Size = new System.Drawing.Size(75, 23);
            this.logout.TabIndex = 3;
            this.logout.Text = "Log Out";
            this.logout.UseVisualStyleBackColor = true;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(265, 90);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 95);
            this.listBox1.TabIndex = 4;
            // 
            // book
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 289);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.logout);
            this.Controls.Add(this.buttonbook);
            this.Controls.Add(this.textBoxuser);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxtime);
            this.Controls.Add(this.comboBoxins);
            this.Name = "book";
            this.Text = "book";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxins;
        private System.Windows.Forms.ComboBox comboBoxtime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxuser;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonbook;
        private System.Windows.Forms.Button logout;
        private System.Windows.Forms.ListBox listBox1;
    }
}