﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class caravi : Form
    {
        public caravi()
        {
            InitializeComponent();
            String usernameQuery = "SELECT NumberPlate From car";
            String dayQuery = "SELECT UserName FROM instructor";

            SQL.editComboBoxItems(comboBoxcar, usernameQuery);
            SQL.editComboBoxItems(comboBoxins, dayQuery);
        }

        private void buttongo_Click(object sender, EventArgs e)
        {
            string username = "", car = "";

            //Check that the text boxes has something typed in it using a method


            try
            {
                /*YOUR CODE HERE*/
                username = comboBoxins.Text.Trim();
                car = comboBoxcar.Text.Trim();
               
            }
            catch
            {
                //Error message, more useful when you are storing numbers etc. into the database.
                MessageBox.Show("Please make sure your text is in correct format.");
                return;
            }

            //(2) Execute the INSERT statement, making sure all quotes and commas are in the correct places.
            //      Practice first on SQL Server Management Studio to make sure it is entering the correct data and in the correct format,
            //      then copy across the statement and where there are string replace the actual text for the variables stored above.
            //Example query: " INSERT INTO Users VALUES ('jkc1', 'John', 'Middle', 'Carter', 'pass1') "
            try
            {
                SQL.executeQuery("INSERT INTO car_avi VALUES ('" + username + "', '" + car + "')");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }



            //success message for the user to know it worked//
            MessageBox.Show("car set to ('" + username + "')");
        }

        private void buttonback_Click(object sender, EventArgs e)
        {
            Hide();
            adminconsole adminconsole = new adminconsole();
            adminconsole.ShowDialog();
            Close();
        }
    }
}
