﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class Adminpage : Form
    {
        public Adminpage()
        {
            InitializeComponent();
            textBox2.PasswordChar = '*';
            textBox2.MaxLength = 20;         //max textbox character count


        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
            LoginPage login = new LoginPage();
            login.ShowDialog();
            Close();
        }

        private void Loginad_Click(object sender, EventArgs e)
        {
            //Variables to be used: 1x bool, 4x string
            bool loggedIn = false;
            string username = "", firstname = "", lastname = "", password = "";

            //check if boxes are empty, the Trim removes white space in text from either side
            if ("".Equals(textBox1.Text.Trim()) || "".Equals(textBox2.Text.Trim()))
            {
                MessageBox.Show("Please make sure you enter a Username and Password");
                return;
            }

            //(1) GET the username and password from the text boxes, is good to put them in a try catch
            try
            {
                /*YOUR CODE HERE*/

                username = textBox1.Text.Trim();
                password = textBox2.Text.Trim();



            }
            catch
            {
                //Error message, more useful when you are storing numbers etc. into the database.
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }

            //(2) SELECT statement getting all data from users, i.e. SELECT * FROM Users
            /*YOUR CODE HERE*/

            SQL.selectQuery("SELECT * FROM admin");


            //(3) IF it returns some data, THEN check each username and password combination, ELSE There are no registered users
            /*YOUR CODE HERE*/
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (username.Equals(SQL.read[0].ToString()) && password.Equals(SQL.read[5].ToString()))
                    {
                        loggedIn = true;
                        firstname = SQL.read[1].ToString();
                        lastname = SQL.read[1].ToString();
                        break;
                    }
                }
            }

            else
            {
                MessageBox.Show("No users have been registered");
                return;
            }

            //if logged in display a success message
            if (loggedIn)
            {

                Hide();
                adminconsole adminconsole = new adminconsole();
                adminconsole.ShowDialog();
                Close();

            }
            else
            {
                //message stating we couldn't log in
                MessageBox.Show("Login attempt unsuccessful! Please check details");
                textBox1.Focus();
                return;
            }

        }


        /// <summary>
        /// When clicked on switch page to the register page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labelRegister_Click(object sender, EventArgs e)
        {
            //Hides the login page form from user
            this.Hide();
            //Create a Register Page object to change to
            RegisterPage register = new RegisterPage();
            //show the register page
            register.ShowDialog();
            //close the login page we are currently on
            this.Close();
        }
        public void initialiseTextBoxes()
        {
            //goes through and clears all of the textboxes
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    (c as TextBox).Clear();
                }
            }
            //makes next place user types the text box
            textBox1.Focus();
        }
    }
}

