﻿namespace INTRO_USERS
{
    partial class caravi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxcar = new System.Windows.Forms.ComboBox();
            this.comboBoxins = new System.Windows.Forms.ComboBox();
            this.buttongo = new System.Windows.Forms.Button();
            this.buttonback = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(53, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "CAR";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 156);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "INSTRUCTOR";
            // 
            // comboBoxcar
            // 
            this.comboBoxcar.FormattingEnabled = true;
            this.comboBoxcar.Location = new System.Drawing.Point(222, 89);
            this.comboBoxcar.Name = "comboBoxcar";
            this.comboBoxcar.Size = new System.Drawing.Size(121, 21);
            this.comboBoxcar.TabIndex = 2;
            // 
            // comboBoxins
            // 
            this.comboBoxins.FormattingEnabled = true;
            this.comboBoxins.Location = new System.Drawing.Point(222, 158);
            this.comboBoxins.Name = "comboBoxins";
            this.comboBoxins.Size = new System.Drawing.Size(121, 21);
            this.comboBoxins.TabIndex = 2;
            // 
            // buttongo
            // 
            this.buttongo.Location = new System.Drawing.Point(307, 258);
            this.buttongo.Name = "buttongo";
            this.buttongo.Size = new System.Drawing.Size(75, 23);
            this.buttongo.TabIndex = 3;
            this.buttongo.Text = "GO";
            this.buttongo.UseVisualStyleBackColor = true;
            this.buttongo.Click += new System.EventHandler(this.buttongo_Click);
            // 
            // buttonback
            // 
            this.buttonback.Location = new System.Drawing.Point(16, 258);
            this.buttonback.Name = "buttonback";
            this.buttonback.Size = new System.Drawing.Size(75, 23);
            this.buttonback.TabIndex = 3;
            this.buttonback.Text = "BACK";
            this.buttonback.UseVisualStyleBackColor = true;
            this.buttonback.Click += new System.EventHandler(this.buttonback_Click);
            // 
            // caravi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 293);
            this.Controls.Add(this.buttonback);
            this.Controls.Add(this.buttongo);
            this.Controls.Add(this.comboBoxins);
            this.Controls.Add(this.comboBoxcar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Name = "caravi";
            this.Text = "caravi";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxcar;
        private System.Windows.Forms.ComboBox comboBoxins;
        private System.Windows.Forms.Button buttongo;
        private System.Windows.Forms.Button buttonback;
    }
}