﻿namespace INTRO_USERS
{
    partial class Adminpage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.useradmin = new System.Windows.Forms.Label();
            this.passadmin = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.Loginad = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(90, 93);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(194, 20);
            this.textBox1.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(90, 145);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(194, 20);
            this.textBox2.TabIndex = 2;
            // 
            // useradmin
            // 
            this.useradmin.AutoSize = true;
            this.useradmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.useradmin.Location = new System.Drawing.Point(135, 58);
            this.useradmin.Name = "useradmin";
            this.useradmin.Size = new System.Drawing.Size(87, 20);
            this.useradmin.TabIndex = 15;
            this.useradmin.Text = "Username:";
            this.useradmin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // passadmin
            // 
            this.passadmin.AutoSize = true;
            this.passadmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passadmin.Location = new System.Drawing.Point(135, 122);
            this.passadmin.Name = "passadmin";
            this.passadmin.Size = new System.Drawing.Size(82, 20);
            this.passadmin.TabIndex = 15;
            this.passadmin.Text = "Password:";
            this.passadmin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(230, 273);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 27);
            this.button1.TabIndex = 1;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button2_Click);
            // 
            // Loginad
            // 
            this.Loginad.Location = new System.Drawing.Point(142, 210);
            this.Loginad.Name = "Loginad";
            this.Loginad.Size = new System.Drawing.Size(75, 23);
            this.Loginad.TabIndex = 16;
            this.Loginad.Text = "LogIn";
            this.Loginad.UseVisualStyleBackColor = true;
            this.Loginad.Click += new System.EventHandler(this.Loginad_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(115, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 25);
            this.label1.TabIndex = 17;
            this.label1.Text = "ADMIN LOGIN";
            // 
            // Adminpage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 312);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Loginad);
            this.Controls.Add(this.passadmin);
            this.Controls.Add(this.useradmin);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Name = "Adminpage";
            this.Text = "Adminpage";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label useradmin;
        private System.Windows.Forms.Label passadmin;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Loginad;
        private System.Windows.Forms.Label label1;
    }
}