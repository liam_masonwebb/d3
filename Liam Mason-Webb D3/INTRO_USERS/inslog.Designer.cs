﻿namespace INTRO_USERS
{
    partial class inslog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.username1 = new System.Windows.Forms.TextBox();
            this.password2 = new System.Windows.Forms.TextBox();
            this.insuser = new System.Windows.Forms.Label();
            this.inspass = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.Back = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // username1
            // 
            this.username1.Location = new System.Drawing.Point(140, 121);
            this.username1.Name = "username1";
            this.username1.Size = new System.Drawing.Size(198, 20);
            this.username1.TabIndex = 0;
            // 
            // password2
            // 
            this.password2.Location = new System.Drawing.Point(140, 195);
            this.password2.Name = "password2";
            this.password2.Size = new System.Drawing.Size(198, 20);
            this.password2.TabIndex = 0;
            // 
            // insuser
            // 
            this.insuser.AutoSize = true;
            this.insuser.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insuser.Location = new System.Drawing.Point(173, 79);
            this.insuser.Name = "insuser";
            this.insuser.Size = new System.Drawing.Size(122, 24);
            this.insuser.TabIndex = 1;
            this.insuser.Text = "USER NAME";
            // 
            // inspass
            // 
            this.inspass.AutoSize = true;
            this.inspass.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inspass.Location = new System.Drawing.Point(186, 159);
            this.inspass.Name = "inspass";
            this.inspass.Size = new System.Drawing.Size(92, 24);
            this.inspass.TabIndex = 1;
            this.inspass.Text = "Password";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(190, 240);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 36);
            this.button1.TabIndex = 2;
            this.button1.Text = "LOGIN";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Back
            // 
            this.Back.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Back.Location = new System.Drawing.Point(382, 351);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(77, 28);
            this.Back.TabIndex = 3;
            this.Back.Text = "Back";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // inslog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 391);
            this.Controls.Add(this.Back);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.inspass);
            this.Controls.Add(this.insuser);
            this.Controls.Add(this.password2);
            this.Controls.Add(this.username1);
            this.Name = "inslog";
            this.Text = "inslog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox username1;
        private System.Windows.Forms.TextBox password2;
        private System.Windows.Forms.Label insuser;
        private System.Windows.Forms.Label inspass;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Back;
    }
}