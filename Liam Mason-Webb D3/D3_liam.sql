
use D3_Liam_Mason_Webb

CREATE TABLE client (
	UserName varchar(20)  PRIMARY KEY,
	FirstName varchar(20) NOT NULL,
	Email varchar(20) NOT NULL ,
	LastName varchar(20) NOT NULL,
	Password varchar(20) ,
	Phone int NOT NULL 
	
)

CREATE TABLE instructor (
	UserName varchar(20)  PRIMARY KEY,
	FirstName varchar(20) NOT NULL,
	Email varchar(20) NOT NULL ,
	LastName varchar(20) NOT NULL,
	Password varchar(20) ,
	Phone int , 
	
)


CREATE TABLE admin (
	UserName varchar(20) NOT NULL PRIMARY KEY,
	FirstName varchar(20) NOT NULL,
	LastName varchar(20) NOT NULL,
	Email varchar(20) NOT NULL ,
	Phone int  ,
	Password varchar(20)
)



CREATE TABLE car (
	NumberPlate varchar (20) NOT NULL PRIMARY KEY,
	Make varchar(20) NOT NULL,	
)


CREATE TABLE type (
	name varchar (20) NOT NULL PRIMARY KEY,
	cost varchar(20) NOT NULL,
	hours varchar(20) NOT NULL,	
)



CREATE TABLE timeslot (
	name varchar (20) NOT NULL PRIMARY KEY,
	cost varchar(20) NOT NULL,
	hours varchar(20) NOT NULL,	
)



CREATE TABLE appointment (
	appid varchar (20) NOT NULL PRIMARY KEY,
	notes varchar(20),
	client varchar(20),
	instructor varchar(20),
	timeslot varchar(20),
	FOREIGN KEY (client) REFERENCES client(UserName),	
	FOREIGN KEY (instructor) REFERENCES instructor(UserName),
	FOREIGN KEY (timeslot) REFERENCES timeslot(name)

)

 
CREATE TABLE document (
	docid varchar (20) NOT NULL PRIMARY KEY,
	date date NOT NULL ,
	type varchar (20) NOT NULL ,
	link varchar (20) NOT NULL,
    FOREIGN KEY (link) REFERENCES client(UserName)

		
)


Create Table avalbility (	
	availid varchar (20) NOT NULL PRIMARY KEY,
	instructor varchar(20),
	timeslot varchar(20),
	FOREIGN KEY (instructor) REFERENCES instructor(UserName),
	FOREIGN KEY (timeslot) REFERENCES timeslot(name)
)



INSERT INTO avalbility VALUES ('123', 'lmw', 'MONDAY_0700')

INSERT INTO instructor VALUES ('lmwj', 'John', 'Doe', 'jdoe@gmail.com','lmd', '0275623554')
INSERT INTO instructor VALUES ('inst2', 'pete', 'carter', 'pcarter@gmail.com','Password1*', '0275623554')
INSERT INTO instructor VALUES ('inst3', 'jen', 'scott', 'jscott@gmail.com','Password1*', '0275623554')
INSERT INTO instructor VALUES ('inst4', 'matt', 'harry', 'mharry@gmail.com','Password1*', '0275623554')

INSERT INTO client VALUES ('jdoe', 'John', 'jdoe@gmail.com', 'doe','Password1*', '0275623554')
INSERT INTO client VALUES ('pcarter', 'pete', 'pcarter@gmail.com', 'carter','Password1', '0275623554')
INSERT INTO client VALUES ('jscott', 'jen', 'jscott@gmail.com', 'scott','Password1', '0275623554')
INSERT INTO client VALUES ('mharry', 'matt', 'mharry@gmail.com', 'harry','Password1', '0275623554')

INSERT INTO admin VALUES ('lmw', 'pete', 'carter', 'pcarter@gmail.com','0275623554', 'lmw')
INSERT INTO admin VALUES ('admin3', 'jen', 'scott', 'jscott@gmail.com','0275623554', 'Password1*')
INSERT INTO admin VALUES ('admin4', 'matt', 'harry', 'mharry@gmail.com','0275623554', 'Password1*')

INSERT INTO car VALUES ('DAE256', 'NISSAN')
INSERT INTO car VALUES ('FEP269', 'TOYOTA')
INSERT INTO car VALUES ('GHT658', 'SUBARU')
INSERT INTO car VALUES ('AMS619', 'NISSAN')
INSERT INTO car VALUES ('DHE4256', 'KIA')
--monday--
INSERT INTO timeslot VALUES ('MONDAY_0700', '$60', '1')
INSERT INTO timeslot VALUES ('MONDAY_0800', '$60', '1')
INSERT INTO timeslot VALUES ('MONDAY_0900', '$60', '1')
INSERT INTO timeslot VALUES ('MONDAY_1000', '$60', '1')
INSERT INTO timeslot VALUES ('MONDAY_1100', '$60', '1')
INSERT INTO timeslot VALUES ('MONDAY_1200', '$60', '1')
INSERT INTO timeslot VALUES ('MONDAY_1300', '$60', '1')
INSERT INTO timeslot VALUES ('MONDAY_1400', '$60', '1')
INSERT INTO timeslot VALUES ('MONDAY_1500', '$60', '1')
INSERT INTO timeslot VALUES ('MONDAY_1600', '$60', '1')
INSERT INTO timeslot VALUES ('MONDAY_1700', '$60', '1')
INSERT INTO timeslot VALUES ('MONDAY_1800', '$60', '1')
INSERT INTO timeslot VALUES ('MONDAY_1900', '$60', '1')
INSERT INTO timeslot VALUES ('MONDAY_2000', '$60', '1')
--tuesday--
INSERT INTO timeslot VALUES ('TUESDAY_0700', '$60', '1')
INSERT INTO timeslot VALUES ('TUESDAY_0800', '$60', '1')
INSERT INTO timeslot VALUES ('TUESDAY_0900', '$60', '1')
INSERT INTO timeslot VALUES ('TUESDAY_1000', '$60', '1')
INSERT INTO timeslot VALUES ('TUESDAY_1100', '$60', '1')
INSERT INTO timeslot VALUES ('TUESDAY_1200', '$60', '1')
INSERT INTO timeslot VALUES ('TUESDAY_1300', '$60', '1')
INSERT INTO timeslot VALUES ('TUESDAY_1400', '$60', '1')
INSERT INTO timeslot VALUES ('TUESDAY_1500', '$60', '1')
INSERT INTO timeslot VALUES ('TUESDAY_1600', '$60', '1')
INSERT INTO timeslot VALUES ('TUESDAY_1700', '$60', '1')
INSERT INTO timeslot VALUES ('TUESDAY_1800', '$60', '1')
INSERT INTO timeslot VALUES ('TUESDAY_1900', '$60', '1')
INSERT INTO timeslot VALUES ('TUESDAY_2000', '$60', '1')
--wednesday--
INSERT INTO timeslot VALUES ('WEDNESDAY_0700', '$60', '1')
INSERT INTO timeslot VALUES ('WEDNESDAY_0800', '$60', '1')
INSERT INTO timeslot VALUES ('WEDNESDAY_0900', '$60', '1')
INSERT INTO timeslot VALUES ('WEDNESDAY_1000', '$60', '1')
INSERT INTO timeslot VALUES ('WEDNESDAY_1100', '$60', '1')
INSERT INTO timeslot VALUES ('WEDNESDAY_1200', '$60', '1')
INSERT INTO timeslot VALUES ('WEDNESDAY_1300', '$60', '1')
INSERT INTO timeslot VALUES ('WEDNESDAY_1400', '$60', '1')
INSERT INTO timeslot VALUES ('WEDNESDAY_1500', '$60', '1')
INSERT INTO timeslot VALUES ('WEDNESDAY_1600', '$60', '1')
INSERT INTO timeslot VALUES ('WEDNESDAY_1700', '$60', '1')
INSERT INTO timeslot VALUES ('WEDNESDAY_1800', '$60', '1')
INSERT INTO timeslot VALUES ('WEDNESDAY_1900', '$60', '1')
INSERT INTO timeslot VALUES ('WEDNESDAY_2000', '$60', '1')
--THURSDAY--
INSERT INTO timeslot VALUES ('THURSDAY_0700', '$60', '1')
INSERT INTO timeslot VALUES ('THURSDAY_0800', '$60', '1')
INSERT INTO timeslot VALUES ('THURSDAY_0900', '$60', '1')
INSERT INTO timeslot VALUES ('THURSDAY_1000', '$60', '1')
INSERT INTO timeslot VALUES ('THURSDAY_1100', '$60', '1')
INSERT INTO timeslot VALUES ('THURSDAY_1200', '$60', '1')
INSERT INTO timeslot VALUES ('THURSDAY_1300', '$60', '1')
INSERT INTO timeslot VALUES ('THURSDAY_1400', '$60', '1')
INSERT INTO timeslot VALUES ('THURSDAY_1500', '$60', '1')
INSERT INTO timeslot VALUES ('THURSDAY_1600', '$60', '1')
INSERT INTO timeslot VALUES ('THURSDAY_1700', '$60', '1')
INSERT INTO timeslot VALUES ('THURSDAY_1800', '$60', '1')
INSERT INTO timeslot VALUES ('THURSDAY_1900', '$60', '1')
INSERT INTO timeslot VALUES ('THURSDAY_2000', '$60', '1')
--FRIDAY--
INSERT INTO timeslot VALUES ('FRIDAY_0700', '$60', '1')
INSERT INTO timeslot VALUES ('FRIDAY_0800', '$60', '1')
INSERT INTO timeslot VALUES ('FRIDAY_0900', '$60', '1')
INSERT INTO timeslot VALUES ('FRIDAY_1000', '$60', '1')
INSERT INTO timeslot VALUES ('FRIDAY_1100', '$60', '1')
INSERT INTO timeslot VALUES ('FRIDAY_1200', '$60', '1')
INSERT INTO timeslot VALUES ('FRIDAY_1300', '$60', '1')
INSERT INTO timeslot VALUES ('FRIDAY_1400', '$60', '1')
INSERT INTO timeslot VALUES ('FRIDAY_1500', '$60', '1')
INSERT INTO timeslot VALUES ('FRIDAY_1600', '$60', '1')
INSERT INTO timeslot VALUES ('FRIDAY_1700', '$60', '1')
INSERT INTO timeslot VALUES ('FRIDAY_1800', '$60', '1')
INSERT INTO timeslot VALUES ('FRIDAY_1900', '$60', '1')
INSERT INTO timeslot VALUES ('FRIDAY_2000', '$60', '1')
--SATURDAY--
INSERT INTO timeslot VALUES ('SATURDAY_0700', '$60', '1')
INSERT INTO timeslot VALUES ('SATURDAY_0800', '$60', '1')
INSERT INTO timeslot VALUES ('SATURDAY_0900', '$60', '1')
INSERT INTO timeslot VALUES ('SATURDAY_1000', '$60', '1')
INSERT INTO timeslot VALUES ('SATURDAY_1100', '$60', '1')
INSERT INTO timeslot VALUES ('SATURDAY_1200', '$60', '1')
INSERT INTO timeslot VALUES ('SATURDAY_1300', '$60', '1')
INSERT INTO timeslot VALUES ('SATURDAY_1400', '$60', '1')
INSERT INTO timeslot VALUES ('SATURDAY_1500', '$60', '1')
INSERT INTO timeslot VALUES ('SATURDAY_1600', '$60', '1')
INSERT INTO timeslot VALUES ('SATURDAY_1700', '$60', '1')
INSERT INTO timeslot VALUES ('SATURDAY_1800', '$60', '1')
INSERT INTO timeslot VALUES ('SATURDAY_1900', '$60', '1')
INSERT INTO timeslot VALUES ('SATURDAY_2000', '$60', '1')

INSERT INTO avalbility VALUES ('INST2_SATURDAY_2000', '', 'SATURDAY_2000')



Select * FROM client
Select * FROM instructor
Select * FROM car
Select * FROM admin


SELECT * FROM avalbility







